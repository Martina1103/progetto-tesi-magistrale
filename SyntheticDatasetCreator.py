import numpy as np

def createDataset(sampled_points, n):
    dataset = []
    #print(sampled_points.shape)
    for points in sampled_points:
        if points.shape[0] == 0 :
            feature = np.zeros(n, dtype=int)
        else:
            idx = np.random.choice(points.shape[0], n, replace=True)
            #noise = np.random.normal(0, 3, n)
            #print(noise.shape, noise[0])
            feature = points[idx] # + noise
        dataset.append(feature)

    dataset = np.array(dataset)
    dataset = dataset.T.reshape(-1, dataset.shape[0])
    return dataset

def createDatasetFromFreatureDict(feature_dict, n):
    dataset = []

    for i in feature_dict.keys():
        data = np.array(feature_dict[i])
        idx = np.random.choice(data.shape[0], n, replace=True)
        dataset.append(data[idx])

    dataset = np.array(dataset)
    dataset = dataset.T.reshape(-1, dataset.shape[0])
    return dataset

def divideDataset(dataset, interval):
    test_idx = np.arange(0, dataset.shape[0], interval)

    test = dataset[test_idx]
    train = np.delete(dataset, test_idx, axis=0)

    return train, test
