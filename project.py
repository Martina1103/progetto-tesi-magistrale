import numpy as np
import lightgbm as lgb
import math
import json
import pickle
import matplotlib.pyplot as plt

import load_dataset
import GamExtractor
import TreeUtils
import InfoFromThresholds
import SyntheticDatasetCreator

# --------- CONFIGURATION ------------------ #

dataset = 'houses'

if dataset == 'concrete':
    basic_path = '/home/martina/Scrivania/Tesi/models/real/'
    n_features = 8
    important_features = [5, 6, 4, 1]
    interactions = [[0,7], [0,3]]
if dataset == 'years':
    basic_path = '/home/martina/Scrivania/Tesi/models/real2/'
    n_features = 90
    important_features = [2,5,4,12,6,22]
    interactions = [[0,1]]
if dataset == 'houses':
    basic_path = '/home/martina/Scrivania/Tesi/models/houses2/'
    n_features = 18
    important_features = [17, 0, 12, 10, 4, 18]
    interactions = [[15,16], [3,15]]
if dataset == 'synth':
    basic_path = '/home/martina/Scrivania/Tesi/models/'
    n_features = 6
    important_features = [0,1,2,3,4,5]
    interactions = []
if dataset == 'inter-synth':
    basic_path = '/home/martina/Scrivania/Tesi/models/interaction-synth/'
    n_features = 6
    important_features = [0,1,2,5]
    interactions = [[3,4]]


use_toy_example = False
# labels
if use_toy_example:
    n_features = 6
    data_labels = ['x0', 'x1', 'x2', 'x3', 'x4', 'x5']
else:
    data_labels = []
    for i in range(0,n_features):
        data_labels.append('x' + str(i))

plot_dataset_features = False


############### F O R E S T ###################
train_tree = False
if train_tree:
    tree_params = {
        'objective':'regression',
        'num_leaves': 32
    }
    tree_num_round = 10000
    early_stopping_rounds = 50
    tree_verbose_eval = True

extract_features = False
if extract_features:
    show_extracted_features = False

    # metodi per estrarre dataset da foresta
    use_equal_distribution = True
    use_max_steps = False
    use_kmeans = True
    use_max_kmeans = False
    use_mean_points = True

features_interaction = False
############## e n d ### f o r e s t ####################


############ G A M ##################
show_gams_info = False

GAMs_from_dataset = False
if GAMs_from_dataset:
    save_gam_dataset = False
    show_gams_plots_dataset = True

GAMs_from_forest = True
if GAMs_from_forest:
    create_dataset = False
    save_gam_forest = False
    show_gams_plots_forest = False
    compare_with_dataset_GAM = False
    compare_with_original_dataset =  False
    compare_sampled_with_forest_gam = False
    compare_gams = False

######### e n d ## G A M ###########


method_labels = {
    'k-means': 'k-means',
    'mean_points': 'equi_size',
    'equal_distribution': 'equi_width',
    'all': 'all'
}

# ---------- END CONFIGURATION -------- #


print('Load dataset...')
xs_train, ys_train, xs_test, ys_test = load_dataset.loadDataset(n_features = n_features, dataset = dataset, toy_example=use_toy_example, plot_response_hist=False)
print(xs_train.shape, xs_test.shape)
print(np.min(ys_train), np.max(ys_train))

if GAMs_from_dataset:
    print('Extracting GAMs from dataset...')

    rmse_all = []
    for nodes in [35]:
        if save_gam_dataset:
            gamdataset = GamExtractor.getGAM(xs_train, ys_train, data_labels, nodes, dataset, interactions= True)
            with open(basic_path + 'gams_dataset/nodes-'+str(nodes), 'wb') as f:
                pickle.dump(gam_dataset, f)
        else:
            with open(basic_path + 'gams_dataset/nodes-'+str(nodes), 'rb') as f:
                gam_dataset = pickle.load(f)

        rmse, nrmse = GamExtractor.getRMSE(gam_dataset, xs_test, ys_test)
        rmse_all.append(rmse)
        print(str(nodes) + " & " + str(round(rmse, 5)) + " & " + str(round(nrmse, 6)) + " \\\\ ")

        if show_gams_plots_dataset:
            GamExtractor.plotGAMsimple(gam_dataset, path=basic_path + 'gams_dataset/graphs/nodes' + str(nodes) + '-',
                                       save=True, interaction= True, interaction_idx= [0,1])

        #interactions
        # gam_dataset = GamExtractor.getGAM(xs_train, ys_train, data_labels, nodes, interactions= True)
        # rmse, nrmse = GamExtractor.getRMSE(gam_dataset, xs_test, ys_test)
        # rmse_all.append(rmse)
        # print(str(nodes) + " & " + str(round(rmse, 5)) + " & " + str(round(nrmse, 6)) + " \\\\ ")
        # GamExtractor.plotGAMsimple(gam_dataset, path=basic_path + 'gams_dataset/graphs/interaction-nodes' + str(nodes) + '-',
        #                            save=True, interaction= True, interaction_idx= [0,1])


if train_tree:
    train_data = lgb.Dataset(xs_train, label=ys_train)
    validation_data = lgb.Dataset(xs_test, label=ys_test, reference=train_data)

    lgbm_info = {}
    bst = lgb.train(tree_params,
                    train_data,
                    tree_num_round,
                    early_stopping_rounds=early_stopping_rounds,
                    feature_name=['f' + str(i + 1) for i in range(xs_train.shape[-1])],
                    valid_sets=[validation_data],
                    evals_result=lgbm_info,
                    verbose_eval=tree_verbose_eval)

    bst.save_model(basic_path + 'forest/forest-2-log.txt', num_iteration=bst.best_iteration)
    #print(bst.dump_model())

    response = bst.predict(xs_test)
    error = ys_test - response
    mse = np.mean(np.square(error))
    rmse = np.sqrt(mse)
    print('RMSE TREE:', rmse, rmse / (np.max(ys_test) - np.min(ys_test)))

    feature_importance = np.argsort(bst.feature_importance())[::-1]

    plt.figure()
    #plt.plot(np.arange(0, feature_importance.shape[0]), bst.feature_importance()[feature_importance])
    plt.plot(bst.feature_importance()[feature_importance], feature_importance)
    plt.title('Features importance')
    plt.show()

    # 0 1 2 5 // 12 22 4 6 35 9 77 57 ...
    print('feature_importance', bst.feature_importance(), feature_importance, bst.feature_importance()[feature_importance])


if extract_features:
    feature_dict = TreeUtils.getTreeThresholds(bst.dump_model(), n_features)
    json.dump(feature_dict, open(basic_path + 'forest/thresholds-forest-2-log.txt', 'w'))


    feature_dict = json.load(open(basic_path + 'forest/thresholds-forest-2-log.txt'))
    for n_samples_from_thresholds in [20,40,60,80,100,150,200]:
        if use_kmeans:
            method = 'k-means'
            sampled = InfoFromThresholds.useKmeans(feature_dict, n_samples_from_thresholds)
            np.save(basic_path + 'forest/' + method + '_' + str(n_samples_from_thresholds) + '_forest2-log', sampled)

        if use_max_steps:
            method = 'max_steps'
            sampled = InfoFromThresholds.useMaxSteps(feature_dict, n_samples_from_thresholds)
            np.save(basic_path + 'forest/' + method + '_' + str(n_samples_from_thresholds) + '_forest2-log', sampled)

        if use_equal_distribution:
            method = 'equal_distribution'
            sampled = InfoFromThresholds.useEqualDistribution(feature_dict, n_samples_from_thresholds)
            np.save(basic_path + 'forest/' + method + '_' + str(n_samples_from_thresholds) + '_forest2-log', sampled)

        if use_max_kmeans:
            method = 'max_kmeans'
            sampled = InfoFromThresholds.useMaxKmeans(feature_dict, n_samples_from_thresholds)
            np.save(basic_path + 'forest/' + method + '_' + str(n_samples_from_thresholds) + '_forest2-log', sampled)

        if use_mean_points:
            method = 'mean_points'
            sampled = InfoFromThresholds.useMeanPoints(feature_dict, n_samples_from_thresholds)
            np.save(basic_path + 'forest/' + method + '_' + str(n_samples_from_thresholds) + '_forest2-log', sampled)

if features_interaction:
    bst = lgb.Booster(model_file= basic_path + 'forest/forest-2-log.txt')
    plot = lgb.plot_importance(bst, max_num_features=18)
    plt.show()
    print('important features:',  np.argsort(bst.feature_importance())[::-1])

    TreeUtils.countInteractions(bst.dump_model())


def evaluateGAMForest(methods, Ns, nodes_all, dataset, save_model = False, fixed_test = False, xs_test = None, ys_test = None):
    plt.figure()
    for method in methods:
        print('Method:', method)
        rmse_nodes = []
        Ns = ['all'] if method == 'all' else Ns
        for N in Ns:
             #N = 'all' if method == 'all' else N
            for nodes in nodes_all:
                #print(nodes)
                if save_model:
                    synthetic_train = np.load(
                        basic_path + 'datasets/log-xs_train-50-' + method + '-N' + str(N) + '.npy')
                    y_train = np.load(
                        basic_path + 'datasets/log-ys_train-50-' + method + '-N' + str(N) + '.npy')

                    # print('datasets/log-xs_train-50-' + method + '-N' + str(N) + '.npy', synthetic_train[0])

                    gam_forest = GamExtractor.getGAM(synthetic_train, y_train, data_labels, nodes, dataset, interactions=False)
                    with open(basic_path + 'gams_forest/log-2-' + method + '-50_N' + str(N) + '-' + str(nodes), 'wb') as f:
                        pickle.dump(gam_forest, f)
                else:
                    with open(basic_path + 'gams_forest/log-2-' + method + '-50_N' + str(N) + '-' + str(nodes), 'rb') as f:
                        gam_forest = pickle.load(f)
                print('GAM_LAMBDA', gam_forest.lam)
                # rmse_all = np.zeros(len(methods) * len(Ns) * 5)
                if not fixed_test:
                    rmse_all = 0
                    for label in ['a', 'b', 'c', 'd', 'e']:
                        xs_test = np.load(basic_path + 'datasets/log-xs_test-' + str(label) + '-' + method + '-N' + str(N) + '.npy')
                        ys_test = np.load(basic_path + 'datasets/log-ys_test-' + str(label) + '-' + method + '-N' + str(N) + '.npy')

                        rmse, nrmse = GamExtractor.getRMSE(gam_forest, xs_test, ys_test)
                        rmse_round = round(rmse, 5)

                        rmse_all = rmse_all + rmse_round
                    rmse = rmse_all / 5
                else:
                    rmse, nrmse = GamExtractor.getRMSE(gam_forest, xs_test, ys_test)
                rmse_nodes.append(rmse)

            # MEAN OF 5 ITERATION
        if method == 'all':
            #plt.plot(Ns, rmse_nodes, label=method_labels[method])
            plt.hlines(rmse_nodes[0], 20, 200, linestyles='dashed', label= method_labels[method], colors='red')
        else:
            plt.plot(Ns, rmse_nodes, label= method_labels[method])
        #print('Ns:', Ns)
        #print('rmse_nodes: ', rmse_nodes)
        row = method_labels[method]
        for node in rmse_nodes:
            row = row + ' & ' + str(round(node, 3))
        row = row + ' \\\\ '
        print(row)

    plt.title('Dataset: houses - no interaction')
    plt.legend()
    plt.xlabel('sample size N')
    plt.ylabel('RMSE')
    plt.axis((20, 200, 0.1, 0.3))
    plt.savefig(basic_path + 'gams_forest/graphs/log-best-rmse-no-interaction-2.png')
    plt.show()


if GAMs_from_forest:

    if create_dataset:
        print("Dataset from forest...")
        bst = lgb.Booster(model_file= basic_path + 'forest/forest-2-log.txt')


        for method in ['k-means', 'mean_points', 'equal_distribution']:
            print('Method:', method)
            for N in [20, 40, 60, 80, 100, 150, 200]:
                print("N:", N, basic_path + 'forest/' + method + '_' + str(N) + '_forest2-log.npy')
                sampled = np.load(basic_path + 'forest/' + method + '_' + str(N) + '_forest2-log.npy', allow_pickle=True)
                synthetic_train = SyntheticDatasetCreator.createDataset(sampled, 45000)
                y_train = bst.predict(synthetic_train)
                np.save(basic_path + 'datasets/log-xs_train-50-' + method + '-N' + str(N), synthetic_train)
                np.save(basic_path + 'datasets/log-ys_train-50-' + method + '-N' + str(N), y_train)
                #print('datasets/log-ys_train-50-' + method + '-N' + str(N), np.min(y_train),
                #      np.max(y_train))
                for label in ['a', 'b', 'c', 'd', 'e']:
                    synthetic_test = SyntheticDatasetCreator.createDataset(sampled, 5000)
                    tree_y_test = bst.predict(synthetic_test)
                    np.save(basic_path + 'datasets/log-xs_test-'+str(label)+'-'+method+'-N'+str(N), synthetic_test)
                    np.save(basic_path + 'datasets/log-ys_test-'+str(label)+'-'+method+'-N'+str(N), tree_y_test)


        sampled = json.load(open(basic_path + 'forest/thresholds-forest-2-log.txt', 'r'))
        method = 'all'
        N = 'all'
        synthetic_train = SyntheticDatasetCreator.createDatasetFromFreatureDict(sampled, 20000)
        y_train = bst.predict(synthetic_train)
        np.save(basic_path + 'datasets/log-xs_train-50-' + method + '-N' + str(N), synthetic_train)
        np.save(basic_path + 'datasets/log-ys_train-50-' + method + '-N' + str(N), y_train)
        for label in ['a', 'b', 'c', 'd', 'e']:
            synthetic_test = SyntheticDatasetCreator.createDatasetFromFreatureDict(sampled, 2000)
            tree_y_test = bst.predict(synthetic_test)
            np.save(basic_path + 'datasets/log-xs_test-' + str(label) + '-' + method + '-N' + str(N), synthetic_test)
            np.save(basic_path + 'datasets/log-ys_test-' + str(label) + '-' + method + '-N' + str(N), tree_y_test)

    # GAM FOREST vs FOREST + loading gams
    print('GAM FOREST vs FOREST')

    evaluateGAMForest(
        methods= ['equal_distribution', 'k-means', 'mean_points', 'all'],
        Ns = [20, 40, 60, 80, 100, 150, 200],
        nodes_all= ['best'],
        dataset = dataset,
        save_model = save_gam_forest
    )

    # evaluateGAMForest(
    #     methods=['all'],
    #     Ns=['all'],
    #     nodes_all = ['best'],
    #     dataset=dataset,
    #     save_model = save_gam_forest
    # )

    if show_gams_plots_forest:
        print('GET PICTURES')
        with open(basic_path + 'gams_forest/inter-k-means-50_N100-25', 'rb') as f:
            gam_forest = pickle.load(f)
        GamExtractor.plotGAMsimple(gam_forest, basic_path + 'gams_forest/graphs/inter-k-means-50_N100-25', save=True, interaction_idx= [0,1], interaction=True)
        # GamExtractor.compareGAMsPlot(gam_forest, gam_dataset, synthetic_train)


    if compare_with_dataset_GAM:
        print('GAM FOREST vs GAM DATASET')

        print('retrieve best gam from original data...')

        N = 35
        with open(basic_path + 'gams_dataset/nodes-' + str(N), 'rb') as f:
            gam_dataset = pickle.load(f)

        # gam_dataset_response = np.load(basic_path + 'gams_dataset/response-nodes-20.npy')
        gam_dataset_response = np.array(gam_dataset.predict(xs_test)),

        evaluateGAMForest(
            methods= ['equal_distribution', 'k-means', 'mean_points'],
            Ns = [20, 40, 80, 100],
            nodes_all= [10, 15, 20, 25, 40],
            dataset=dataset,
            save_model = save_gam_forest,
            fixed_test= True,
            xs_test = xs_test,
            ys_test = gam_dataset_response
        )

        evaluateGAMForest(
            methods= ['all'],
            Ns = ['all'],
            nodes_all= [10, 15, 20, 25, 40],
            dataset=dataset,
            save_model = save_gam_forest,
            fixed_test= True,
            xs_test = xs_test,
            ys_test = gam_dataset_response
        )



    if compare_with_original_dataset:
        print('GAM FOREST vs ORIGINAL DATASET')

        evaluateGAMForest(
            methods= ['equal_distribution', 'k-means', 'mean_points'],
            Ns = [20, 40, 80, 100],
            nodes_all=[10, 15, 20, 25, 40],
            save_model=False,
            dataset=dataset,
            fixed_test=True,
            xs_test=xs_test,
            ys_test=ys_test
        )

        evaluateGAMForest(
            methods=['all'],
            Ns=['all'],
            nodes_all=[10, 15, 20, 25, 40],
            save_model=save_gam_forest,
            dataset=dataset,
            fixed_test=True,
            xs_test=xs_test,
            ys_test=ys_test
        )

        if show_gams_plots_forest:
            print('GET PICTURES')
            with open(basic_path + 'gams_forest/mean_points-200_N80-20', 'rb') as f:
                gam_forest = pickle.load(f)
            GamExtractor.plotGAMsimple(gam_forest, basic_path + 'gams_forest/graphs/mean_points-200_N80-20-', save=True, interactions = True, interaction_idx=[0,1])
            #GamExtractor.compareGAMsPlot(gam_forest, gam_dataset, synthetic_train)

compare_gams = False
if compare_gams:


    with open(basic_path + 'gams_forest/inter-k-means-50_N100-20', 'rb') as f:
        gam_forest = pickle.load(f)
    #GamExtractor.plotGAMsimple(gam_kmeans, basic_path + 'gams_forest/graphs/k-means-200_N100-20-', save=True)

    with open(basic_path + 'gams_dataset/nodes-35', 'rb') as f:
        gam_data = pickle.load(f)
    # with open(basic_path + 'gams_forest/mean_points_N100-20', 'rb') as f:
    #     gam_kmeans_100_50 = pickle.load(f)
    # with open(basic_path + 'gams_forest/equal_distribution_N100-20', 'rb') as f:
    #     gam_eq_100_20 = pickle.load(f)
    #
    #
    GamExtractor.compareGAMsPlot(gam_data, gam_forest, 'gam_data', 'gam_forest', basic_path + 'gam_compare/', interaction = True, interaction_idx = [0,1])
    #GamExtractor.compareGAMsPlot(gam_data_20, gam_kmeans_200_20, 'gam_data_20', 'gam_kmeans_N200_20',
    #                             basic_path + 'gam_compare/')


compare_without_noise = False
if compare_without_noise:
    print('GAM FOREST vs ORIGINAL DATASET (no noise)')
    #for method in ['all']:
    for method in ['equal_distribution', 'k-means', 'mean_points']:
        print('Method:', method)

        #for N in ['all']:
        for N in [20, 40, 80, 200]:
            print("N:", N)
            for nodes in [5, 10, 20, 50]:
                # gam_forest = forest_gams[method + '_' + str(N) + '_' + str(nodes)]
                with open(basic_path + 'gams_forest/' + method + '_N' + str(N) + '-' + str(nodes), 'rb') as f:
                    gam_forest = pickle.load(f)
                rmse, nrmse = GamExtractor.getRMSE(gam_forest, xs_test, ys_test)
                print(str(nodes) + " & " + str(round(rmse, 5)) + " & " + str(round(nrmse, 6)) + " \\\\ ")

plot_best_models = False
if plot_best_models:
    nodes_all = [10, 15, 20, 25, 30, 35, 40]
    # concrete
    # basic_path = '/home/martina/Scrivania/Tesi/models/real/'
    # best = [
    #     ['equal_distribution', 20],
    #     ['k-means', 100],
    #     ['mean_points', 20],
    #     ['all', 'all']
    # ]
    # plt.figure()
    # for pair in best:
    #     rmse_nodes = []
    #     for nodes in nodes_all:
    #         with open(basic_path + 'gams_forest/inter-' + pair[0] + '-50_N' + str(pair[1]) + '-' + str(nodes), 'rb') as f:
    #             gam_forest = pickle.load(f)
    #         rmse_all = 0
    #         for label in ['a', 'b', 'c', 'd', 'e']:
    #             xs_test = np.load(basic_path + 'datasets/xs_test-' + str(label) + '-' + pair[0] + '-N' + str(pair[1]) + '.npy')
    #             ys_test = np.load(basic_path + 'datasets/ys_test-' + str(label) + '-' + pair[0] + '-N' + str(pair[1]) + '.npy')
    #
    #             rmse, nrmse = GamExtractor.getRMSE(gam_forest, xs_test, ys_test)
    #             rmse_round = round(rmse, 5)
    #
    #             rmse_all = rmse_all + rmse_round
    #         rmse = rmse_all / 5
    #         rmse_nodes.append(rmse)
    #     plt.plot(nodes_all, rmse_nodes, label= pair[0] + ' - N = ' + str(pair[1]))
    # plt.title('Best models - dataset: concrete')
    # plt.legend()
    # plt.xlabel('nodes')
    # plt.ylabel('RMSE')
    # plt.axis((10, 40, 2.5, 4.8))
    # plt.savefig(basic_path + 'gams_forest/graphs/compare-best-inter.png')
    # plt.show()


    # # years
    # basic_path = '/home/martina/Scrivania/Tesi/models/real2/'
    # best = [
    #     ['equal_distribution', 80],
    #     ['k-means', 200],
    #     ['mean_points', 200],
    #     ['all', 'all']
    # ]
    # plt.figure()
    # for pair in best:
    #     rmse_nodes = []
    #     for nodes in nodes_all:
    #         with open(basic_path + 'gams_forest/inter-all-' + pair[0] + '-50_N' + str(pair[1]) + '-' + str(nodes), 'rb') as f:
    #             gam_forest = pickle.load(f)
    #         rmse_all = 0
    #         for label in ['a', 'b', 'c', 'd', 'e']:
    #             xs_test = np.load(basic_path + 'datasets/xs_test-' + str(label) + '-' + pair[0] + '-N' + str(pair[1]) + '.npy')
    #             ys_test = np.load(basic_path + 'datasets/ys_test-' + str(label) + '-' + pair[0] + '-N' + str(pair[1]) + '.npy')
    #
    #             rmse, nrmse = GamExtractor.getRMSE(gam_forest, xs_test, ys_test)
    #             rmse_round = round(rmse, 5)
    #
    #             rmse_all = rmse_all + rmse_round
    #         rmse = rmse_all / 5
    #         rmse_nodes.append(rmse)
    #     plt.plot(nodes_all, rmse_nodes, label= pair[0] + ' - N = ' + str(pair[1]))
    # plt.title('Best models - dataset: years')
    # plt.legend()
    # plt.xlabel('nodes')
    # plt.ylabel('RMSE')
    # plt.axis((10, 40, 4.2, 7))
    # plt.savefig(basic_path + 'gams_forest/graphs/compare-best-inter-all.png')
    # plt.show()
    #
    # houses
    # basic_path = '/home/martina/Scrivania/Tesi/models/houses/'
    # best = [
    #     ['equal_distribution', 60],
    #     ['k-means', 100],
    #     ['mean_points', 100],
    #     ['all', 'all']
    # ]
    # plt.figure()
    # for pair in best:
    #     rmse_nodes = []
    #     for nodes in nodes_all:
    #         with open(basic_path + 'gams_forest/no-inter-' + pair[0] + '-50_N' + str(pair[1]) + '-' + str(nodes),
    #                   'rb') as f:
    #             gam_forest = pickle.load(f)
    #         rmse_all = 0
    #         for label in ['a', 'b', 'c', 'd', 'e']:
    #             xs_test = np.load(
    #                 basic_path + 'datasets/xs_test-' + str(label) + '-' + pair[0] + '-N' + str(pair[1]) + '.npy')
    #             ys_test = np.load(
    #                 basic_path + 'datasets/ys_test-' + str(label) + '-' + pair[0] + '-N' + str(pair[1]) + '.npy')
    #
    #             rmse, nrmse = GamExtractor.getRMSE(gam_forest, xs_test, ys_test)
    #             rmse_round = round(rmse, 5)
    #
    #             rmse_all = rmse_all + rmse_round
    #         rmse = rmse_all / 5
    #         rmse_nodes.append(rmse)
    #     plt.plot(nodes_all, rmse_nodes, label=method_labels[pair[0]] + ' - N = ' + str(pair[1]))
    # plt.title('Best models - dataset: houses')
    # plt.legend()
    # plt.xlabel('nodes')
    # plt.ylabel('RMSE')
    # plt.axis((10, 40, 92000, 168000))
    # plt.savefig(basic_path + 'gams_forest/graphs/compare-best-no-inter.png')
    # plt.show()

    # synth
    # basic_path = '/home/martina/Scrivania/Tesi/models/interaction-synth/'
    # best = [
    #     ['equal_distribution', 20],
    #     ['k-means', 20],
    #     ['mean_points', 20],
    #     ['all', 'all']
    # ]
    # plt.figure()
    # for pair in best:
    #     rmse_nodes = []
    #     for nodes in nodes_all:
    #         with open(basic_path + 'gams_forest/inter-' + pair[0] + '-50_N' + str(pair[1]) + '-' + str(nodes),
    #                   'rb') as f:
    #             gam_forest = pickle.load(f)
    #         rmse_all = 0
    #         for label in ['a', 'b', 'c', 'd', 'e']:
    #             xs_test = np.load(
    #                 basic_path + 'datasets/xs_test-' + str(label) + '-' + pair[0] + '-N' + str(pair[1]) + '.npy')
    #             ys_test = np.load(
    #                 basic_path + 'datasets/ys_test-' + str(label) + '-' + pair[0] + '-N' + str(pair[1]) + '.npy')
    #
    #             rmse, nrmse = GamExtractor.getRMSE(gam_forest, xs_test, ys_test)
    #             rmse_round = round(rmse, 5)
    #
    #             rmse_all = rmse_all + rmse_round
    #         rmse = rmse_all / 5
    #         rmse_nodes.append(rmse)
    #     plt.plot(nodes_all, rmse_nodes, label=method_labels[pair[0]] + ' - N = ' + str(pair[1]))
    # plt.title('Best models - dataset: synthetic - interactions')
    # plt.legend()
    # plt.xlabel('nodes')
    # plt.ylabel('RMSE')
    # plt.axis((10, 40, 0, 17))
    # plt.savefig(basic_path + 'gams_forest/graphs/compare-best-inter.png')
    # plt.show()



    # BEST INTERACTION OR NOT
    #basic_path = '/home/martina/Scrivania/Tesi/models/real/'
    plt.figure()
    nodes = 'best'
    method = 'mean_points'
    Ns = [20, 40, 60, 80, 100, 150, 200]

    rmse_nodes = []
    for N in Ns:
        with open(basic_path + 'gams_forest/log-2-' + method + '-50_N' + str(N) + '-' + str(nodes),
                  'rb') as f:
            gam_forest = pickle.load(f)
        rmse_all = 0
        for label in ['a', 'b', 'c', 'd', 'e']:
            xs_test = np.load(
                basic_path + 'datasets/log-xs_test-' + str(label) + '-' + method + '-N' + str(N) + '.npy')
            ys_test = np.load(
                basic_path + 'datasets/log-ys_test-' + str(label) + '-' + method + '-N' + str(N) + '.npy')

            rmse, nrmse = GamExtractor.getRMSE(gam_forest, xs_test, ys_test)
            rmse_round = round(rmse, 5)

            rmse_all = rmse_all + rmse_round
        rmse = rmse_all / 5
        rmse_nodes.append(rmse)

    print('Ns: ', Ns)
    print('rmse_nodes: ', rmse_nodes)
    plt.plot(Ns, rmse_nodes, label='no-interactions')

    pair = ['k-means', 'best']
    method = 'k-means'

    rmse_nodes = []
    for N in Ns:
        with open(basic_path + 'gams_forest/log-inter-15-16-2-' + method + '-50_N' + str(N) + '-' + str(nodes),
                  'rb') as f:
            gam_forest = pickle.load(f)
        rmse_all = 0
        for label in ['a', 'b', 'c', 'd', 'e']:
            xs_test = np.load(
                basic_path + 'datasets/log-xs_test-' + str(label) + '-' + method + '-N' + str(N) + '.npy')
            ys_test = np.load(
                basic_path + 'datasets/log-ys_test-' + str(label) + '-' + method + '-N' + str(N) + '.npy')

            rmse, nrmse = GamExtractor.getRMSE(gam_forest, xs_test, ys_test)
            rmse_round = round(rmse, 5)

            rmse_all = rmse_all + rmse_round
        rmse = rmse_all / 5
        rmse_nodes.append(rmse)
    plt.plot(Ns, rmse_nodes, label='interaction f16-f17')

    pair = ['k-means', 'best']

    method = 'k-means'

    rmse_nodes = []
    for N in Ns:
        with open(basic_path + 'gams_forest/log-inter-double2-' + method + '-50_N' + str(N) + '-' + str(nodes),
                  'rb') as f:
            gam_forest = pickle.load(f)
        rmse_all = 0
        for label in ['a', 'b', 'c', 'd', 'e']:
            xs_test = np.load(
                basic_path + 'datasets/log-xs_test-' + str(label) + '-' + method + '-N' + str(N) + '.npy')
            ys_test = np.load(
                basic_path + 'datasets/log-ys_test-' + str(label) + '-' + method + '-N' + str(N) + '.npy')

            rmse, nrmse = GamExtractor.getRMSE(gam_forest, xs_test, ys_test)
            rmse_round = round(rmse, 5)

            rmse_all = rmse_all + rmse_round
        rmse = rmse_all / 5
        rmse_nodes.append(rmse)
    plt.plot(Ns, rmse_nodes, label='interaction all')

    plt.title('Best models - dataset: houses')
    plt.legend()
    plt.xlabel('sample size N')
    plt.ylabel('RMSE')
    plt.axis((20, 200, 0.12, 0.24))
    plt.savefig(basic_path + 'gams_forest/graphs/log-2-compare-best-absolute-2.png')
    plt.show()

print_best_gam = True
if print_best_gam:
    # concrete
    pair = ['mean_points', 150] # interaction
    nodes = 'best'
    with open(basic_path + 'gams_forest/log-inter-15-16-2-' + pair[0] + '-50_N' + str(pair[1]) + '-' +str(nodes),
              'rb') as f:
        gam_forest = pickle.load(f)
        GamExtractor.plotGAMsimple(gam_forest, path = basic_path + 'gams_forest/graphs/', interaction = True, interaction_idx = [0], save=True)