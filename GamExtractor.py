import pandas as pd
import numpy as np
from matplotlib.legend_handler import HandlerLine2D
from pygam import LinearGAM, s, f, te
import matplotlib.pyplot as plt
import math
from mpl_toolkits import mplot3d

def getGAM(x,y, labels, splines, dataset, interactions: False, show_gams_info=False):
    feats = pd.DataFrame(x, columns=labels)
    target = pd.Series(y)
    if dataset == 'houses':
        if interactions :
           gam = LinearGAM(te(14,15) + s(3) + s(16) + s(12) + s(4) + s(0) + s(17) + s(10)).fit(feats, target)
        else:
           gam = LinearGAM(s(14) + s(15) + s(12) + s(16) + s(0) + s(4) + s(17) + s(3) + s(10)).fit(feats, target)

    if dataset == 'years':
        if interactions:
           gam = LinearGAM(te(0,1) + s(2) + s(5) + s(4) + s(11) + s(12) + s(6) + s(8)).fit(feats, target)
        else:
           gam = LinearGAM(s(2) + s(5) + s(0) + s(1) + s(4) + s(11) + s(12) + s(6) + s(8)).fit(feats, target)

    if dataset == 'concrete':
        if interactions:
           gam = LinearGAM(te(0,7) + s(3) + s(5) + s(6) + s(4) + s(1) + s(2)).fit(feats, target)
        else:
           gam = LinearGAM(s(3) + s(5) + s(0) + s(6) + s(7) + s(4) + s(1) + s(2)).fit(feats, target)

    if dataset == 'inter-synth':
        if interactions:
           gam = LinearGAM(te(3,4) + s(0) + s(1) + s(2) + s(5)).fit(feats, target)
        else:
           gam = LinearGAM(s(0) + s(1) + s(2) + s(3) + s(4) + s(5)).fit(feats, target)

    if  dataset == 'synth':
        if interactions:
           gam = LinearGAM(te(0,2) + s(1) + s(3) + s(4) + s(5)).fit(feats, target)
        else:
           gam = LinearGAM(s(0) + s(1) + s(2) + s(3) + s(4) + s(5)).fit(feats, target)

    #gam = LinearGAM(s(2) + s(5) + s(0) + s(1) + s(6) + s(11) + s(12) + s(4), n_splines=splines).fit(feats, target)
    #gam = LinearGAM(s(0) + s(1) + s(2) + s(5) + te(3,4), n_splines=splines).fit(feats, target)
    if show_gams_info:
        gam.summary()

    return gam


def plotGAMsimple(gam, path = '', interaction = False, interaction_idx = [], save=False):
    labels = {
        # te(14,15) + s(3) + s(16) + s(12) + s(4) + s(0) + s(17) + s(10)
        0: 'interaction (f15,f16) - (lat,long)',
        1: 'f4 - sqft_living',
        2: 'f17 - sqft_living15',
        3: 'f13 - yr_built',
        4: 'f5 - sqft_lot',
        5: 'f1 - date',
        6: 'f18 - sqft_lot15',
        7: 'f11 - sqft_above',
    }

    for i, term in enumerate(gam.terms):
        if term.isintercept:
            continue

        if interaction and i in interaction_idx:
            XX = gam.generate_X_grid(term=i, meshgrid=True)
            Z = gam.partial_dependence(term=i, X=XX, meshgrid=True)
            ax = plt.axes(projection='3d')
            ax.plot_surface(XX[0], XX[1], Z, cmap='viridis')
        else:
            XX_data = gam.generate_X_grid(term=i)
            pdep_data, confi_data = gam.partial_dependence(term=i, X=XX_data, width=0.95)
            plt.figure()
            plt.plot(XX_data[:, term.feature], pdep_data, c='r')
            plt.plot(XX_data[:, term.feature], confi_data, c='blue', ls='--')

        plt.title(labels[i] + ' - houses')
        #plt.title(repr(term) + ' - houses')
        if save:
            plt.savefig(path + 'x' + str(i) + '-2.png')
        plt.show()


def compareGAMsPlot(gam_ref, gam_compare, name_ref, name_compare, file, interaction = False, interaction_idx = []):

    for i, term in enumerate(gam_compare.terms):
        if term.isintercept:
            continue

        if interaction and i in interaction_idx:
            continue
        else:
            XX_data = gam_ref.generate_X_grid(term=i)
            #print('XX_data', XX_data.shape)
            #print(XX_data[0])
            pdep_compare, confi_compare = gam_compare.partial_dependence(term=i, X=XX_data, width=0.95)
            pdep_ref, confi_ref = gam_ref.partial_dependence(term=i, X=XX_data, width=0.95)

            plt.figure()
            line1 = plt.plot(XX_data[:, term.feature], pdep_ref, c='r', label=name_ref)
            plt.plot(XX_data[:, term.feature], confi_ref, c='pink', ls='--')
            line2 = plt.plot(XX_data[:, term.feature], pdep_compare, c='blue', ls='--', label = name_compare)
            plt.plot(XX_data[:, term.feature], confi_compare, c='gray', ls='--')
            # if i in [0,1,2]:
            #     data = np.sort(original[:,0])
            #     response = np.array([func[i](x) for x in data])
            #     plt.plot(data, response, c='green')
            #plt.scatter(xs_train[:, term.feature], ys_train, facecolor='gray', edgecolors='none')


        plt.legend()
        plt.title(repr(term) + ' - ' + name_ref + ' vs. ' + name_compare)
        plt.savefig(file + name_ref + '_vs_' + name_compare + '_x' + str(i) + '.png')
        plt.show()

def getRMSE(gam, xs_test, ys_test):
    response = gam.predict(xs_test)
    error = ys_test - response
    mse = np.mean(np.square(error))
    rmse = np.sqrt(mse)
    return rmse, rmse / (np.max(ys_test) - np.min(ys_test))
