import numpy as np
import matplotlib.pyplot as plt
#import seaborn as sns
import pandas as pd
from datetime import datetime

def loadDataset(n_features, dataset, toy_example=True, plot_response_hist = False):
    if toy_example:
        # LOAD DATASET FROM CSV
        # f(x0,x1) = x0^3 - 12*x1^2 + (z^4)^1/3

        file_train = '/home/martina/PycharmProjects_bkp/dummy-dataset-generator/sin-inter-train-6-50.csv'
        file_test = '/home/martina/PycharmProjects_bkp/dummy-dataset-generator/sin-inter-test-6-50.csv'

        #file_train = '/home/martina/PycharmProjects_bkp/dummy-dataset-generator/dummy-train-6-50.csv'
        #file_test = '/home/martina/PycharmProjects_bkp/dummy-dataset-generator/dummy-test-6-50.csv'

        train_dataset = np.genfromtxt(file_train, delimiter=',')

        xs_train = train_dataset[:, 0:n_features]
        #print(xs_train.shape)
        ys_train = train_dataset[:, n_features]
        #ys_train = ys_train/np.max(ys_train)

        test_dataset = np.genfromtxt(file_test, delimiter=',')
        xs_test = test_dataset[:, 0:n_features]
        ys_test = test_dataset[:, n_features]
        print('dataset', xs_test.shape, ys_test.shape, ys_train[0])
        #ys_test = ys_test / np.max(ys_test)
        #print('max, min y: ', np.max(ys_test), np.min(ys_test))

    else:
        if dataset == 'concrete':
            # LOAD CONCRETE DATASET
            print('loading concrete dataset...')
            concrete = np.genfromtxt('ConcreteDataEng.csv', delimiter=";")

            test_idx = np.arange(0, 1030, 10)

            test_dataset = concrete[test_idx]
            xs_test = test_dataset[:, 0:n_features]
            ys_test = test_dataset[:, n_features]

            train_dataset = np.delete(concrete, test_idx, axis=0)
            xs_train = train_dataset[:, 0:n_features]
            ys_train = train_dataset[:, n_features]

        if dataset == 'years':
            print('loading years dataset...')
            # LOAD YEARS DATASET
            years = pd.read_csv('year_train.csv', sep=',')
            years = np.array(years)
            #subsamples = np.arange(0, dataset.shape[0], 4)
            #dataset = dataset[subsamples,:]
            xs_train = years[:, 1:]
            ys_train = years[:, 0]

            #print('dataset', xs_train.shape, ys_train.shape, ys_train[0])

            years = pd.read_csv('year_test.csv', sep=',')
            years = np.array(years)
            #subsamples = np.arange(0, dataset.shape[0], 4)
            #dataset = dataset[subsamples,:]
            xs_test = years[:, 1:]
            ys_test = years[:, 0]
            #print('dataset', xs_test.shape, ys_test.shape, ys_train[0])

        if dataset == 'houses':
            print('loading houses dataset...')
            # LOAD HOUSE DATASET
            dataset = pd.read_csv('/home/martina/Scrivania/Tesi/models/houses/kc_house_data.csv', sep=',')
            dataset = np.array(dataset)
            print('before', dataset.shape)

            # rimuovo id
            dataset = np.delete(dataset, 0, axis=1)

            #rimuovo zip code
            dataset = np.delete(dataset, 15, axis=1)

            #rimuovo missing values
            dataset = np.delete(dataset, [10,17], axis=0)

            # converto date in timestamp per usarli come interi
            convert_time = lambda x: datetime.strptime(x, '%Y%m%dT%H%M%S').timestamp()
            for data in dataset:
                data[0] = convert_time(data[0])

            # applico log transformation alla response
            # dataset[:,1] = np.log(dataset[:,1])
            print('response shape:', dataset[:,1].shape)
            yy = dataset[:, 1]
            yy = yy.astype(np.int)
            yy = np.log(yy)
            dataset[:,1] = yy

            #rimuovo elementi con response troppo alta
            # yy = dataset[:,1]
            # yy = yy.astype(np.int)
            # a = np.where(yy > np.quantile(yy, .98))
            # dataset = np.delete(dataset, a[0], axis=0)
            # print('after', dataset.shape)

            #prendo circa il 10% del dataset come test
            test_idx = np.arange(0, dataset.shape[0], 10)
            data_test = dataset[test_idx]
            data_train = np.delete(dataset, test_idx, axis=0)

            ys_test = data_test[:, 1]
            xs_test = np.delete(data_test, 1, axis=1)

            ys_train = data_train[:, 1]
            xs_train = np.delete(data_train, 1, axis=1)
            #--- end houses


    if plot_response_hist:
        # sns.distplot(ys_train, hist=True, kde=True,
        #              bins=int(180 / 10), color='darkblue',
        #             hist_kws={'edgecolor': 'black'},
        #             kde_kws={'linewidth': 4})
        plt.figure()
        plt.hist(ys_train)
        plt.title('Response dataset 2')
        plt.show()

    return xs_train, ys_train, xs_test, ys_test