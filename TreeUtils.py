import matplotlib.pyplot as plt
import numpy as np

def collectTreeInfo(tree):
    info = {
        'split': [],
        'threshold': []
    }
    if 'split_feature' in tree:
        info['split'].append(tree['split_feature'])
        info['threshold'].append(tree['threshold'])

        left_info = collectTreeInfo(tree['left_child'])
        right_info = collectTreeInfo(tree['right_child'])

        info['split'] = info['split'] + left_info['split'] + right_info['split']
        info['threshold'] = info['threshold'] + left_info['threshold'] + right_info['threshold']

    return info

def getTreeThresholds(forest, n_features):
    tree_info = []

    for tree in forest['tree_info']:
        tree_info.append(collectTreeInfo(tree['tree_structure']))

    feature_dict = {}

    for i in range(0, n_features):
        feature_dict[i] = []

    for info in tree_info:
        for i in range(0, len(info['split'])):
            feature_dict[info['split'][i]].append(info['threshold'][i])

    return feature_dict

def countInteractions(tree):
    n_features = tree['max_feature_idx'] + 1
    print('in interaction', n_features)
    interactions = np.zeros((n_features, n_features), dtype=int)
    forest = tree['tree_info']
    i = 0
    tot = len(forest)
    thresh = [
        # round((len(forest) / 100) * 0.1),
        # round((len(forest) / 100) * 0.3),
        # round((len(forest) / 100) * 0.5),
        # round((len(forest) / 100) * 1),
        # round((len(forest) / 100) * 5),
        round((len(forest) / 100) * 10),
        # round((len(forest) / 100) * 15),
        # round((len(forest) / 100) * 20),
        # round((len(forest) / 100) * 25),
        # round((len(forest) / 100) * 30),
        # round((len(forest) / 100) * 40),
        # round((len(forest) / 100) * 50)
    ]
    print('how many trees?', len(forest), round(len(forest) / 100), round((len(forest) / 100)*5))
    for tree in forest:
        #print('TREE ', i)
        registerInteractions(tree['tree_structure'], interactions)
        #paths = registerDecisionPaths(tree['tree_structure'], [])
        #countInteractionsInDecisionPaths(paths, interactions)
        #for path in paths:
        #    print(path)

        if i in thresh:
            print(i)
            print(interactions)
            printInteractionMatrix(interactions)
            break
        #if i == 10:
        #     break
        i = i + 1
    #print(interactions)
    #printInteractionMatrix(interactions)
    #result = np.where(interactions == np.amax(interactions))
    #print(list(zip(result[0], result[1])))


def registerDecisionPaths(subtree, path):
    if 'split_feature' in subtree:
        #path.append(subtree['split_feature'])
        #current = subtree['split_feature']
        left_paths = registerDecisionPaths(subtree['left_child'], path)
        right_paths = registerDecisionPaths(subtree['right_child'], path)
        for path in left_paths:
            path.insert(0,subtree['split_feature'])
        for path in right_paths:
            path.insert(0,subtree['split_feature'])
        return left_paths + right_paths
    else: #foglia
        return [[]]


def registerInteractions(subtree, interactions):
    if 'split_feature' in subtree:
        current = subtree['split_feature']
        left = registerInteractions(subtree['left_child'], interactions)
        if left >= 0:
            if left > current:
                interactions[current][left] = interactions[current][left] + 1
            else:
                interactions[left][current] = interactions[left][current] + 1
            #print('internal node', current, left)
        right = registerInteractions(subtree['right_child'], interactions)
        if right >= 0:
            if right > current:
                interactions[current][right] = interactions[current][right] + 1
            else:
                interactions[right][current] = interactions[right][current] + 1
            #print(current, right)
    else: # foglia
        #print('foglia')
        current = -1
    return current

def printInteractionMatrix(interactions):
    important_feats = np.sort([14,15,16,12,4,3,0,17,10])
    #important_feats = np.sort([2,5,0,1,4,11,12,6,8, 9, 13, 16, 35, 67, 77])
    #important_feats = [5,3,0,6,7,4,1]
    #important_feats = [0,1,2,3,4,5,6,7]
    print('\\begin{table}[h!]')
    print('\\centering')
    print('\\begin{tabular}{c|c|c|c|c|c|c|}')
    row = ''
    for i in important_feats:
        row = row + ' & \\textbf{f' + str(i+1) + '}'
    row = row + ' \\\\'
    print(row)
    print('\\hline')
    i = 0
    for feat in important_feats:
        row = '\\textbf{f' + str(feat+1) + '}'
        j = 0
        a = np.array(interactions[feat])
        b = a[important_feats]

        for elem in b:
            if j == i:
                row = row + ' & 0'
            else:
                row = row + ' & ' + str(elem)
            j = j+1
        row = row + ' \\\\'
        print(row)
        print('\\hline')
        i = i+1
    print('\\end{tabular}')
    print('\\caption{\\small }')
    print('\\end{table}')

def countInteractionsInDecisionPaths(paths, interactions):
    for path in paths:
        for i in range(0,interactions.shape[0]):
            j = i + 1
            while j < interactions.shape[0]:
                if i in path and j in path:
                    interactions[i,j] = interactions[i,j] + 1
                j = j + 1
            i = i + 1
    return interactions