import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt


def useEqualDistribution(thresholds, N):
    sampled = []
    for i in thresholds.keys():

        if len(thresholds[i]) > 0:
            min_val = np.min(thresholds[i])
            max_val = np.max(thresholds[i])
            if min_val != max_val:
                equal = np.arange(min_val, max_val, (max_val - min_val) / (N-1))
            else:
                equal = np.array([])
            equal = np.append(equal, [max_val])
        else:
            equal = np.array([])
        sampled.append(equal)

    return sampled

def useMaxSteps(thresholds, N):
    sampled = []
    for i in thresholds.keys():
        data = np.sort(np.array(thresholds[i]))
        sorted_idx = np.argsort(np.diff(data))[0:(N-1)]
        sorted_idx = np.sort(sorted_idx)
        samples = []
        sample_idx = []

        prev = 0
        for idx in sorted_idx:
            samples.append(np.mean(data[prev:idx + 1]))
            sample_idx.append( int( ((idx+1) + prev)/2 ) )
            prev = idx + 1

        if data[prev:-1].shape[0] > 0:
            samples.append(np.mean(data[prev:-1]))
            sample_idx.append( int( (data.shape[0] + prev)/2 ) )

        #print(np.unique(samples).shape)
        #print(np.unique(samples))
        samples = np.array(samples)
        sampled.append(samples)

    return sampled

def useKmeans(thresholds, N):
    # clusters = np.empty(len(thresholds), dtype=int)
    # n = 41
    # for i in feature_importance:
    #     clusters[i] = n
    #     n = n - 10
    #     if n < 0:
    #         n = 1

    sampled = []
    #print('KEYS', thresholds.keys())
    for i in thresholds.keys():
        #print('data', data.shape)
        if len(thresholds[i]) > 0:
            data = np.sort(np.array(thresholds[i])).reshape(-1, 1)
            # kmeans = KMeans(n_clusters=clusters[i], random_state=0).fit(data)
            kmeans = KMeans(n_clusters=min(N, len(thresholds[i])), random_state=0).fit(data)
            centers = np.sort(np.array(kmeans.cluster_centers_.T[0]))
            #print('centers', centers.shape)
            sampled.append(centers)

    return sampled

def useMaxKmeans(thresholds, N):
    sampled = []
    for i in thresholds.keys():
        #print('feat' + str(i), len(thresholds[i]))
        data = np.sort(np.array(thresholds[i]))
        max_idx = np.argsort(np.diff(data))[0:int(len(thresholds[i])/5)]
        max_pts = data[max_idx].reshape(-1, 1)

        if len(thresholds[i]) > 0:
            kmeans = KMeans(n_clusters=N, random_state=0).fit(max_pts)
            centers = np.sort(np.array(kmeans.cluster_centers_.T[0]))
            sampled.append(centers)

    return sampled

def useMeanPoints(thresholds, N):
    sampled = []
    for i in thresholds.keys():
        n = 0
        points = []
        #print('FEATURE ' + str(i))
        step = int(len(thresholds[i]) / N)
        if step > 0:
            data = np.sort(np.array(thresholds[i]))
            for j in range(0, N-1):
                points.append(np.mean(data[n:n+step]))
                n = n + step
            #print(str(len(thresholds[i])) + ' / ' + str(N) + ' = ' + str(step) + ' = ' + str(step * N) )
            #print('empty?', data[n:])
            if data[n:].shape[0] > 0:
                points.append(np.mean(data[n:]))
        else:
            #print('not enough - unique')
            points = np.unique(thresholds[i])
        #print('points', np.array(points).shape)
        sampled.append(np.array(points))
    #print('sampled', len(sampled))
    return sampled



def plotResults(original_data, sampled, feature, func, method):
    data = np.sort(original_data[:, feature])
    #response = ((data ** 4) ** 1 / 3)
    response = np.array([func(x) for x in data])
    plt.figure()
    plt.plot(data, response)
    sampled_response = np.array([func(x) for x in sampled[feature]])
    #sampled_response = ((sampled[feature] ** 4) ** 1 / 3)
    plt.plot(sampled[feature], sampled_response, marker='o', color='r')
    plt.plot(sampled[feature], np.full((sampled[feature].shape[0], 1), - 5 ), marker='o', color="black")
    plt.title('feat: x' + str(feature) + ' - method: ' + method)
    plt.savefig('/home/martina/Scrivania/Tesi/report2/graphs/gam_dataset_2/extracted_figure_x'+str(feature)+'_method_'+method+'.png')
    plt.show()

def compareWithForestGAM(sampled, feature, gam, method, meshgrid):
    plt.figure()
    plt.plot(sampled[feature], np.full((sampled[feature].shape[0], 1), 1960), marker='o', color="black")
    plt.plot(sampled[feature], gam.predict(meshgrid), 'r--')
    plt.plot(sampled[feature], gam.prediction_intervals(meshgrid, width=.95), color='b', ls='--')
    plt.title('gam of feat x' + str(feature) + ' - method: ' + method)
    plt.show()